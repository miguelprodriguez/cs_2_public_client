 let navItems =  document.querySelector("#navSession");
 let navItems2 =  document.querySelector("#navSession2");
 let userToken = localStorage.getItem('token');
 let isAdmin = localStorage.getItem('isAdmin');

 console.log(userToken);
 console.log(isAdmin);

if(!userToken) {
	navItems.innerHTML = 
				`
					<li class="nav-item">
						<a href="./login.html" class="nav-link"> Log in </a>
					</li>

				`

	navItems2.innerHTML = 
				`
					<li class="nav-item">
						<a href="./register.html" class="nav-link"> Register </a>
					</li>

				`
	
} 

else {
	navItems.innerHTML = `
				<li class="nav-item">
					<a href="./logout.html" class="nav-link"> Log Out </a>
				</li>
	`
}


if(isAdmin == "true") {
	navSession2.innerHTML = 
				`<li class="nav-item">
					<a href="./addCourse.html" class="nav-link"> Add Course </a>
				</li>`

} 
else
{
	navSession2.innerHTML = 
			`
					<li class="nav-item">
						<a href="./profile.html" class="nav-link"> Profile </a>
					</li>
				`
}

