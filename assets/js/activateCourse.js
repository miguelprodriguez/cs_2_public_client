let urlId = new URLSearchParams(window.location.search);
let courseId = urlId.get('id');
console.log(courseId);

let token = localStorage.getItem('token');

fetch(`https://guarded-everglades-41766.herokuapp.com/api/courses/${courseId}/activate`, 
	{
    method: 'DELETE',
    headers: {
        'Authorization': `Bearer ${token}`
    }
  })
.then(res => {
             return res.json()
          })
.then(data => {
    console.log(data)
    //creation of new course successful
    if(data === true){
        // redirect to courses index page
        alert('Course has been activated')
        window.location.replace('./courses.html')
    }
    else{
        //error in creating course, redirect to error page
        alert('Something went wrong')
    }
})


