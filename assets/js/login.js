let loginForm = document.querySelector("#loginUser")


loginForm.addEventListener("submit", (e) => {
    e.preventDefault()

    let email = document.querySelector("#userEmail").value
    let password  = document.querySelector("#password").value   
    // console.log(email)
    // console.log(password)

    //lets now create a control structure that will allow us to determine if there is data inserted in the given fields
    if(email == "" || password == "") {
        alert("Please input your email and/or password!")
    } 
    else {
        fetch('https://guarded-everglades-41766.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }, 
            body: JSON.stringify({
               email: email,
               password: password 
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            // check if data has been caught
            console.log(data)
            // successful authentication will return a JWT via response accessToken property
            // determine if the JWT has been generated together with the user credentials in an object format.
            if(data.access){
                //store JWT in the localStorage
                localStorage.setItem('token', data.access); 
                //send a fetch request to decode the JWT and obtain the user ID and role for storing inside the context
                fetch('https://guarded-everglades-41766.herokuapp.com/api/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.access}`
                    }
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    //set the global user state to have properties containing authenticated user ID and Role.
                    console.log(data)
                    localStorage.setItem("id", data._id)
                    localStorage.setItem("isAdmin", data.isAdmin)
                    //Redirect the user to the profile page. 
                    if (data.isAdmin == false) 
                    {
                        window.location.replace("./profile.html")
                    }
                    else
                    {
                         window.location.replace("./courses.html")
                    }
                    
                })
            } else {
                //the else branch will run if there us an authentication failure
                alert("Something Went Wrong, Check your Credentials!")
            }
        })
    }

})