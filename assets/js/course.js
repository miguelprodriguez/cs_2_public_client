let urlId = new URLSearchParams(window.location.search);
let courseId = urlId.get('id');
console.log(courseId);

const token = localStorage.getItem('token')

let courseName = document.querySelector("#courseName");
let coursePrice = document.querySelector("#coursePrice");
let courseDescription = document.querySelector("#courseDescription");
let enrollContainer = document.querySelector("#enrollContainer");
let studentsList = document.querySelector("#studentsList");

function enroll(){
	fetch('https://guarded-everglades-41766.herokuapp.com/api/users/enroll', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId: courseId
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		alert('You have enrolled on the course')
		window.location.replace("./profile.html")
	})
}

if (!token || token === null) 
{
	alert("You must login first")
	window.location.replace("./login.html")
}

if(isAdmin == "true")
{
	fetch(`https://guarded-everglades-41766.herokuapp.com/api/courses/${courseId}`, 
		{
			method: 'GET',
			headers: 
			{
				'Content-Type': 'application/json'
			}
		})
			.then(res => res.json())
			.then(data => 
			{
				console.log(data)

				if (data.enrollees == 0)
				{
					courseName.innerHTML = `${data.name}`
					coursePrice.innerHTML = `₱${data.price} <br><br> Active: ${data.isActive}`
					courseDescription.innerHTML = `${data.description}`
					enrollContainer.innerHTML = 
						`<a href="./editCourse.html?id=${data._id}" class="btn btn-custom fixed">Edit</a>
						<a href="./deleteCourse.html?id=${data._id}" class="btn btn-custom fixed">Delete</a>`
				}	

				else 
				{
					for (i = 0; i < data.enrollees.length; i++)
					{
						courseName.innerHTML = `${data.name}`
						coursePrice.innerHTML = `₱${data.price} <br><br> Active: ${data.isActive ? "yes" : "No"}`
						courseDescription.innerHTML = `${data.description}`
						enrollContainer.innerHTML = 
						`<a href="./archiveCourse.html?id=${data._id}" class="btn btn-custom">${data.isActive ? "Archive" : "Activate"}</a>
						<a href="./editCourse.html?id=${data._id}" class="btn btn-custom fixed">Edit</a>
						<a href="./deleteCourse.html?id=${data._id}" class="btn btn-custom fixed">Delete</a>`
					}
					for(i = 0; i < data.enrolleesName.length; i++)
					{
						studentsHeader.innerHTML = `<h1>Students Enrolled</h1>`
						studentsList.innerHTML += `<li>${data.enrolleesName[i].firstName} ${data.enrolleesName[i].lastName}</li>`
					}

				}
			})
}

else
{
	fetch(`https://guarded-everglades-41766.herokuapp.com/api/courses/${courseId}`, 
		{
			headers: 
			{
				Authorization: `Bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data => 
			{
				console.log(data)

				courseName.innerHTML = `${data.name}`
				coursePrice.innerHTML = `₱${data.price}`
				courseDescription.innerHTML = `${data.description}`
				enrollContainer.innerHTML = `<button class="btn btn-custom fixed" id="btn1">Enroll</button>`
				enrollContainer.addEventListener("click", enroll); 

			})
}


