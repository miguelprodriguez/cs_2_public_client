const token = localStorage.getItem('token')
console.log(token)
let courseContainer = document.querySelector('#courseContainer');
let addCourseBtn = document.querySelector('#addCourseBtn');

if(isAdmin == "true")
{
	addCourseBtn.innerHTML = 
		`<div class="container" style="font-family: Oswald">
			<a href="./addCourse.html" style="text-decoration: none"><button class="btn btn-custom btn-block">Add course</button></a>
		</div>`;
	fetch('https://guarded-everglades-41766.herokuapp.com/api/courses/admin', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			let courseArray = data;
			// courseArray.map(course => {course.name})
			for (let i = 0; i < courseArray.length; i++)
			{
					courseContainer.innerHTML += 
					// isActive to put to show only active courses
					`<div class="col-md-4 mx-0" style="font-family: Oswald">
					  <div class="card-body text-center text-white">
					    <h5 class="card-title"><strong>${courseArray[i].name}</strong></h5>
					    <h6 class="card-subtitle mb-2">₱ ${courseArray[i].price}</h6>
					    <p class="card-text">${courseArray[i].description}</p>
					    <p class="card-text">Active: ${courseArray[i].isActive ? "Yes" : "No"}</p>
					    
					    <a href="./course.html?id=${courseArray[i]._id}" class="btn btn-custom fixed">See More</a>
					    <span>
					    	${courseArray[i].isActive 
						    	? 
						    		`<a href="./archiveCourse.html?id=${courseArray[i]._id}" class="btn btn-custom">Archive</a>`
						    	:
						    		`<a href="./activateCourse.html?id=${courseArray[i]._id}" class="btn btn-custom">Activate</a>`
						    }
					    </span>
					   	<a href="./editCourse.html?id=${courseArray[i]._id}" class="btn btn-custom fixed">Edit</a>
					   	<span>
					   		${courseArray[i].isActive
						   		?
						   			""
						   		:
						   			`<a href="./deleteCourse.html?id=${courseArray[i]._id}" class="btn btn-custom fixed">Delete</a>`
					   		}
					   	</span>
					   </div>
					</div>`	
			}
		})
}

else
{
	fetch('https://guarded-everglades-41766.herokuapp.com/api/courses/', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(dataUser => {
			console.log(dataUser)
			let activeCourses = dataUser
			for (let j = 0; j < activeCourses.length; j++)
			{
					console.log(activeCourses[j].name)
					courseContainer.innerHTML += 
					// isActive to put to show only active courses
					`<div class="col-md-4 mx-0" style="font-family: Oswald">
					  <div class="card-body text-center text-white">
					    <h5 class="card-title"><strong>${activeCourses[j].name}</strong></h5>
					    <h6 class="card-subtitle mb-2">₱ ${activeCourses[j].price}</h6>
					    <p class="card-text">${activeCourses[j].description}</p>
					    <a href="./course.html?id=${activeCourses[j]._id}" class="btn btn-custom fixed">See More</a>
					  </div>
					</div>`	
			}
		})
}





