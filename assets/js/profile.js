const token = localStorage.getItem('token')
console.log(token)
let profileContainer = document.querySelector('#profileContainer');

if (!token || token === null) {
	alert("You must login first")
	window.location.replace("./login.html")
}
else {
	// fetch('https://guarded-everglades-41766.herokuapp.com/api/users/details', {
	fetch('https://guarded-everglades-41766.herokuapp.com/api/users/details', {
		headers: {
			Authorization: `Bearer ${token}`
		}
	})

		.then(res => res.json())
		.then(data => {
			console.log(data)

			const enrollmentData = data.enrollments.map((classData, i) => {
				console.log(classData)

				return (
					`
				 	<tr>
				 	   <td>${classData.courseId.name}</td> 
				 	   <td>${(data.enrolledOn[i]).split('T')[0]}</td> 
				 	   <td>${classData.status}</td> 
				 	</tr> 
				 	`
				)
			})

				.join('')
			profileContainer.innerHTML =
				`   <div class="col-md-3 my-5 text-white">
					<img src="../assets/user.webp" class="img-fluid" alt="">
				</div>
				<div class="col-md-4 my-5 text-white">
					<section style="font-family: Oswald">
						<h3 class="text-gray">
							Name:
						</h3>
						<h3>
							${data.firstName}
							${data.lastName}
						</h3>
						<h3 class="text-gray">
							Email address:
						</h3>
						<h3>
							${data.email}
						</h3>
						<h3 class="text-gray">
							Mobile No.:
						</h3>
						<h3>
							${data.mobileNo}
						</h3>
					</section>
				</div>
				<div className="jumbotron col-md-4" style="padding-left:20px; border-left: 1px solid #ccc;">
					<h3 class="text-white"style="font-family: Oswald">
						Class History
					</h3>
					<table class="table text-white">
						<thead>
							<tr>
								<th> Course Name </th>
								<th> Enrolled On </th>
								<th> Status </th>
							</tr>
							<tbody>
								${enrollmentData}
							</tbody>
						</thead>
					</table>
				</div>`
		})
}

