let urlId = new URLSearchParams(window.location.search);
let courseId = urlId.get('id');
console.log(courseId);

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")


fetch(`https://guarded-everglades-41766.herokuapp.com/api/courses/${courseId}`, {
		headers:
		{
			'Content-Type': 'application/json'
		}
	})
		.then(res => {
			return res.json()
		})
		.then(data => {
			console.log(data)

			name.placeholder = data.name
			price.placeholder = data.price
			description.placeholder = data.description

			document.querySelector("#editCourse").addEventListener("submit", (e) => {
				e.preventDefault()

				let courseName = name.value
				let desc = description.value
				let priceValue = price.value

				let token = localStorage.getItem('token')
				fetch('https://guarded-everglades-41766.herokuapp.com/api/courses', {
				// fetch('http://localhost:4000/api/courses', {
					method: 'PUT',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						name: courseName,
						description: desc,
						price: priceValue,
						courseId: courseId //because the endpoint of fetch is not on the specific course
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					console.log(data)
					if(data === true)
					{
						alert("New course has been edited successfully");
						window.location.replace('./courses.html')
					} 
					else 
					{
						alert("Something went wrong in the addition")
					}
				})
				// window.location.replace("./courses.html")
			})

		})
