// console.log("hello from batch register.html")

let registerForm = document.querySelector("#registerUser");


registerForm.addEventListener('submit', (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let email = document.querySelector("#userEmail").value;
	let mobileNo = document.querySelector("#mobileNumber").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

// Validation to enable the submit all fields are registered and if both passwords match.
	if ((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNo.length === 11)) 
	{
		// Check for duplicate emails
		fetch('https://guarded-everglades-41766.herokuapp.com/api/users/email-exists', {
			method: 'POST', 
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({  //method that converts a JS object value into a JSON string.
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			// control structure to see if there are no duplicates found.
			if(data === false){
				fetch('https://guarded-everglades-41766.herokuapp.com/api/users', {
					method: 'POST', 
					headers: {
						'Content-Type': 'application/json'	
					}, 
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					console.log(data)
					// if registration becomes successful.
					if(data === true){
						alert("New account has registered successfully")	
						// after alert, redirect the user to the login page.
						window.location.replace("./login.html")
					} else {
						alert("Something went wrong in the registration")
					}
				})
			}
		})
	} else{
		alert("Something went wrong, Check your credentials.")
	}
})

