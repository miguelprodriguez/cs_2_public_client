 let navItems =  document.querySelector("#navSession");
 let navItems2 =  document.querySelector("#navSession2");
 let userToken = localStorage.getItem('token');
 let isAdmin = localStorage.getItem('isAdmin');

 console.log(navItems);
 console.log(userToken);
 console.log(isAdmin);

if(!userToken) {
	navItems.innerHTML = 
				`
					<li class="nav-item">
						<a href="pages/login.html" class="nav-link"> Log in </a>
					</li>

				`
} else {
	navItems.innerHTML = `
				<li class="nav-item">
					<a href="pages/logout.html" class="nav-link"> Log Out </a>
				</li>
	`
}

if(!userToken) {
	navItems2.innerHTML = 
				`
					<li class="nav-item">
						<a href="pages/register.html" class="nav-link"> Register </a>
					</li>

				`
} 

if(isAdmin == "true") {
	navItems2.innerHTML = 
				``
} 
else
{
	navItems2.innerHTML = 
			`
				<li class="nav-item">
					<a href="pages/profile.html" class="nav-link"> Profile </a>
				</li>
			`
}