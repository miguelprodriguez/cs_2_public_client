let token = localStorage.getItem('token')

document.querySelector("#createCourse").addEventListener('submit', (e) => {
	e.preventDefault()

	let name = document.querySelector("#courseName").value
	let price = document.querySelector("#coursePrice").value
	let description = document.querySelector("#courseDescription").value

	if (courseName !== '' && coursePrice !== '' && courseDescription !== '')
	{
		fetch('https://guarded-everglades-41766.herokuapp.com/api/courses', {
		method: 'POST', 
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
			}, 
		body: JSON.stringify({
			name: name,
			price: price,
			description: description,
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			console.log(data)
			if(data === true)
			{
				alert("New course has been added successfully");
				window.location.replace('./courses.html')
			} 
			else 
			{
				alert("Something went wrong in the addition")
			}
		})
	}
})